package hrn

import "math/rand"

var (
	prefix = [...]string{"sleepy", "lazy", "sneezy", "happy", "boring"}
	pet    = [...]string{"gopher", "fish", "cat", "dog", "bird", "snake"}
)

func PetName(separator string) string {
	return prefix[rand.Intn(len(prefix))] + separator + pet[rand.Intn(len(pet))]
}
